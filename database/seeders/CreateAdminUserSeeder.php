<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'PRO1 Developer', 
            'email' => 'pro1@mail.com',
            'ph_no' => '09777048321',
            'password' => bcrypt('123456')
        ]);
    
        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'customer']);
     
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);
    }
}