<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    //     Schema::create('suppliers', function (Blueprint $table) {
    //         $table->id();
    //         $table->timestamps();
    //     });
        Schema::create('vendor', function (Blueprint $table) {
            $table->id();
            // $table->bigInteger('vendor_id');
            $table->string('vendor_code');
            $table->string('vendor_name');
            // $table->renameColumn('vendor_id', 'id')->autoIncrement();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor');
    }
}
