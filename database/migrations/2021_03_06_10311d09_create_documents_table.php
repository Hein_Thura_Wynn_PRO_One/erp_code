<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {
        Schema::table('documents', function (Blueprint $table) {
            // $table->tinyInteger('status')->nullable();

        });
        Schema::dropIfExists('documents');

        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->string('document_no')->nullable();
            $table->longText('pcode_id')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->bigInteger('prepared_by')->nullable();
            $table->datetime('prepared_at')->nullable();
            $table->bigInteger('checked_by')->nullable();
            $table->datetime('checked_at')->nullable();
            $table->bigInteger('approved_by')->nullable();
            $table->datetime('approved_at')->nullable();
            $table->bigInteger('exported_by')->nullable();
            $table->datetime('exported_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
