<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_category_id')->nullable();
            $table->bigInteger('product_group_id')->nullable();
            $table->bigInteger('product_pattern_id')->nullable();
            $table->bigInteger('product_design_id')->nullable();
            $table->string('main_category')->nullable();
            $table->string('product_category_code')->nullable();
            $table->string('product_category_name')->nullable();
            $table->string('product_group_code')->nullable();
            $table->string('product_group_name')->nullable();
            $table->string('product_pattern_code')->nullable();
            $table->string('product_pattern_name')->nullable();
            $table->string('product_design_code')->nullable();
            $table->string('product_design_name')->nullable();
            $table->string('type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
