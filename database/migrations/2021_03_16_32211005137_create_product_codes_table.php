<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    //    Schema::table('product_codes', function (Blueprint $table) {
    //      $table->tinyInteger('product_grade_id')->nullable();

    //    });

        Schema::create('product_codes', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type');
            $table->string('product_code_no')->nullable();
            $table->tinyInteger('product_grade_id')->nullable();
            $table->string('product_name');
            $table->bigInteger('category_id');
            $table->bigInteger('group_id');
            $table->bigInteger('pattern_id');
            $table->bigInteger('design_id');
            $table->bigInteger('unit_id');
            $table->bigInteger('brand_id');
            $table->bigInteger('supplier_id');
            $table->tinyInteger('product_pack_flag');
            $table->bigInteger('doc_no')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_codes');
    }
}
