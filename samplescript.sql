insert into master_data.master_product(
        lotserialexpireflag,
        ---(0)
        product_pack_flag,
        --(P,S,G)
        vartype,
        commission_flag,
        product_type_flag,
        inactive,
        xposflag,
        xpostime,
        editflag,
        discflag,
        priceflag,
        cardcredit_charge,
        product_id,
        -- product_code,
        product_name1,
        product_name2,
        product_name_eng1,
        product_bill_name,
        -- main_product_unit_id,
        -- product_category_id,
        -- product_group_id,
        -- product_pattern_id,
        -- product_design_id,
        -- product_brand_id,
        -- product_type_id,
        product_name_eng2
    )
values(
        '0',
        'G',
        '1',
        'N',
        'G',
        'A',
        'Y',
        now(),
        'N',
        'Y',
        'N',
        true,
,
        '8010303030309',
        'Min Hex Key Spanner Size-14mm,22mm,27mm A00303',
        'Min Hex Key Spanner Size-14mm,22mm,27mm A00303',
        'Min Hex Key Spanner Size-14mm,22mm,27mm A00303',
        'Min Hex Key Spanner Size-14mm,22mm,27mm A00303',
        1,
        6,
        48,
        219,
        220,
        1935,
        '3',
        'added on 2019/03/23'
    );
insert into master_data.master_product_multiunit(
        product_id,
        product_code,
        product_unit_id,
        product_unit_rate,
        --(0)
        storage_unit_flag,
        stock_unit_flag
    )
values(, '8010303030309', 1, 1, true, true);
insert into master_data.master_product_multivendor(
        product_id,
        list_no,
        product_code,
        barcode_code,
        vendor_id,
        product_unit_id
    )
values(, 1, '8010303030309', '8010303030309', 110, 1);
insert into master_data.master_product_multiprice(
        product_id,
        product_code,
        barcode_code,
        product_unit_id,
        branch_id,
        listno,
        product_price1,
        --(0)
        product_price2,
        --(0)
        product_price3,
        product_price4,
        product_price5,
        product_price6,
        product_price7,
        product_price8,
        product_price9,
        product_price10
    )
values(
,
        '8010060206023',
        '8010060206023',
        1,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    );
insert into master_data.master_product(lotserialexpireflag,product_pack_flag,vartype,commission_flag,product_type_flag,inactive,xposflag,xpostime,editflag,discflag,priceflag,cardcredit_charge,product_id,product_code,product_name1,product_name2,product_name_eng1,product_bill_name,main_product_unit_id,product_category_id,product_group_id,product_pattern_id,product_design_id,product_brand_id,product_type_id) values(0,'P',1,'N','G','A','Y',now(),'N','Y','N',true,( select max(product_id+1) from master_data.master_product),,'foc','foc','foc','foc ',1,8,75,416,422,1,'3' );
<br>

insert into master_data.master_product_multiunit (product_id,product_code,product_unit_id,product_unit_rate,storage_unit_flag,stock_unit_flag ) values((select max(product_id) from master_data.master_product),,1,1,true,true);
<br>

insert into master_data.master_product_multivendor (product_id,list_no,product_code,barcode_code,vendor_id,product_unit_id) values((select max(product_id) from master_data.master_product),1, ,,1,1);
<br>

insert into master_data.master_product_multiprice (product_id,product_code,barcode_code,product_unit_id,branch_id,listno,product_price1,product_price2,product_price3,product_price4,product_price5,product_price6,product_price7,product_price8,product_price9,product_price10) values((select max(product_id) from master_data.master_product),,,1,1,1,0,0,0,0,0,0,0,0,0,0);\




insert into master_data.master_product(lotserialexpireflag,product_pack_flag,vartype,commission_flag,product_type_flag,inactive,xposflag,xpostime,editflag,discflag,priceflag,cardcredit_charge,product_id,product_code,product_name1,product_name2,product_name_eng1,product_bill_name,main_product_unit_id,product_category_id,product_group_id,product_pattern_id,product_design_id,product_brand_id,product_type_id) values(0,'G',1,'N','G','A','Y',now(),'N','Y','N',true,( select max(product_id+1) from master_data.master_product),,'foc gp','foc gp','foc gp','foc gp ',1,8,75,416,422,1,'3' );<br>
insert into master_data.master_product_multiunit (product_id,product_code,product_unit_id,product_unit_rate,storage_unit_flag,stock_unit_flag ) values((select max(product_id) from master_data.master_product),,1,1,true,true);<br>
insert into master_data.master_product_multivendor (product_id,list_no,product_code,barcode_code,vendor_id,product_unit_id) values((select max(product_id) from master_data.master_product),1, ,,1,1);<br>
insert into master_data.master_product_multiprice (product_id,product_code,barcode_code,product_unit_id,branch_id,listno,product_price1,product_price2,product_price3,product_price4,product_price5,product_price6,product_price7,product_price8,product_price9,product_price10) values((select max(product_id) from master_data.master_product),,,1,1,1,0,0,0,0,0,0,0,0,0,0);<br>
insert into master_data.master_product(lotserialexpireflag,product_pack_flag,vartype,commission_flag,product_type_flag,inactive,xposflag,xpostime,editflag,discflag,priceflag,cardcredit_charge,product_id,product_code,product_name1,product_name2,product_name_eng1,product_bill_name,main_product_unit_id,product_category_id,product_group_id,product_pattern_id,product_design_id,product_brand_id,product_type_id) values(0,'G',1,'N','G','A','Y',now(),'N','Y','N',true,( select max(product_id+1) from master_data.master_product),,'foc gp','foc gp','foc gp','foc gp ',1,8,75,416,422,1,'3' );<br>
insert into master_data.master_product_multiunit (product_id,product_code,product_unit_id,product_unit_rate,storage_unit_flag,stock_unit_flag ) values((select max(product_id) from master_data.master_product),,1,1,true,true);<br>
insert into master_data.master_product_multivendor (product_id,list_no,product_code,barcode_code,vendor_id,product_unit_id) values((select max(product_id) from master_data.master_product),1, ,,1,1);<br>
insert into master_data.master_product_multiprice (product_id,product_code,barcode_code,product_unit_id,branch_id,listno,product_price1,product_price2,product_price3,product_price4,product_price5,product_price6,product_price7,product_price8,product_price9,product_price10) values((select max(product_id) from master_data.master_product),,,1,1,1,0,0,0,0,0,0,0,0,0,0)