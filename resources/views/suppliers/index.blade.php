@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Suppliers</h2>
        </div>
        <div class="pull-right mb-3">
            @can('supplier-create')
            <a class="btn btn-success" href="{{ route('suppliers.create') }}"> Add New supplier</a>
            @endcan
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Supplier Code</th>
        <th>Supplier Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($suppliers as $supplier)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $supplier->vendor_code }}</td>
        <td>{{ $supplier->vendor_name }}</td>
      
        <td>
            <form action="{{ route('suppliers.destroy',$supplier->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('suppliers.show',$supplier->id) }}">Show</a>
                @can('supplier-edit')
                <a class="btn btn-primary" href="{{ route('suppliers.edit',$supplier->id) }}">Edit</a>
                @endcan
                @csrf
                @method('DELETE')
                @can('supplier-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
{!! $suppliers->links() !!}

@endsection