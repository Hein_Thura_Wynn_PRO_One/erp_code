@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show product Pattern</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('product_Patterns.index') }}"> Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Pattern Id:</strong>
                {{ $product_Pattern->product_pattern_id }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Pattern Code:</strong>
                {{ $product_Pattern->product_pattern_code }}    
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Pattern Name:</strong>
                {{ $product_Pattern->product_pattern_name }}    
            </div>
        </div>
    </div>
@endsection