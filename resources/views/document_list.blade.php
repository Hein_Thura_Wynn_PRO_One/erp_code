<!DOCTYPE html>
@extends('layouts.app')

@section('content')


<div class="row mb-3">
    <div class="col-md-12 text-center py-3 my-2 bg-light">
        <h1 class="text-uppercase align-center m-auto">My Requested Documents</h1>
    </div>
</div>
<div class="p-3">
    <div class="bg-light p-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif


        <!-- ///DataTable/// -->

        <div class="bg-light p-3">
            <table class="table table-bordered data-table" id="data_table">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Document No</th>
                        <th>Item</th>
                        <th>Prepared By</th>
                        <th>Prepared At</th>
                        <th>Checked By</th>
                        <th>Checked At</th>
                        <th>Approved By</th>
                        <th>Approved At</th>
                        <th>Exported By</th>
                        <th>Exported At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection
@section('js')

<script>
    /////Datatable////
    $(function() {
        var table = $('#data_table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('document_list')}}",
            columns: [{
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'document_no',
                    name: 'document_no'
                },
                // {
                //     data: 'category_name',
                //     name: 'category_name'
                // },
                {
                    data: 'pcode_id',
                    name: 'pcode_id'
                },
                {
                    data: 'prepared_by',
                    name: 'prepared_by'
                },
                {
                    data: 'prepared_at',
                    name: 'prepared_at'
                },
                {
                    data: 'checked_by',
                    name: 'checked_by'
                },
                {
                    data: 'checked_at',
                    name: 'checked_at'
                },
                {
                    data: 'approved_by',
                    name: 'approved_by'
                },
                {
                    data: 'approved_at',
                    name: 'approved_at'
                },
                {
                    data: 'exported_by',
                    name: 'exported_by'
                },
                {
                    data: 'exported_at',
                    name: 'exported_at'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
    });
</script>

@endsection