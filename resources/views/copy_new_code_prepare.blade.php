<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="row mb-3 ">
    <div class="col-md-12  text-center py-3 my-2 bg-light"> <h1 class="text-uppercase align-center m-auto">Prepare New ERP Code </h1>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-9 ">
        <div class="bg-light p-3">
            <div class="row mb-3">
                <div class="col-3">Document No</div>
                <div class="col-9">{{'NCR_03_02_2020-0001'}}</div>
            </div>
            <div class="row mb-3">
                <div class="col-3">User Name</div>
                <div class="col-9"> {{ Auth::user()->name }} </div>
            </div>
            <div class="row mb-3">
                <div class="col-3">Document Date</div>
                <div class="col-9">{{now()}}</div>
                <!-- <div class="col-6"><input class="form-control" type="date" name="doc_date" id="doc_date" value="2020-02-03" min="2000-01-01" max="2020-02-12" /></div> -->
            </div>
            <div class="row mb-3">
                <div class="col-3">Approved by </div>
                <div class="col-9">-</div>
            </div>
            <div class="row mb-3">
                <div class="col-3">Approved Date </div>
                <div class="col-9">-</div>
            </div>
            <div class="row mb-3">
                <div class="col-3">Attach File</div>
                <div class="col-9"><input class="form-control" type="file" name="exchange_no_file"
                        id="exchange_no_file" /></div>
            </div>
            <div class="row mb-3">
                <div class="col-3">Remark</div>
                <div class="col-9"><textarea type="text" name="remark" id="remark" class="form-control"></textarea>
                </div>
            </div>
        </div>
    </div>

    <!-- offset-md-6   offset-lg-9  -->
    <div class="col-md-6 col-sm-12 col-lg-3">
        <form action="" class="table-responsive">
            <table class="table table-striped table-hover form-control my-3">
                <thead>
                    <th scope="col" colspan=2 class="-uppercase text-center">Return Exchange Product</th>

                </thead>

                <tbody>
                    <tr>
                        <td>
                            <span class="ps-5"></span>
                            <span class="ps-5"></span>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <th><small>Now you are login as: </small></th>
                                    <td class="ps-3"> Khin Sandar Aye </td>
                                </tr>
                                <tr>
                                    <th><small>Date Time : </small>
                                    <td class="ps-3"> <em>2020-02-03</em> </td>
                                </tr>
                                <tr>
                                    <td> <button class="btn btn-success m-3">Save</button>

                                    </td>
                                    <td>
                                        <button class="btn btn-danger m-3">Cancel</button>

                                    </td>
                                </tr>


                            </table>
                        </td>

                    </tr>

                </tbody>
            </table>
    </div>
    </form>
</div>

<table class="table table-striped table-hover table-bordered bg-white table-responsive">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Point Type</th>
            <th scope="col">Product Type</th>
            <th scope="col">Product Code</th>
            <th scope="col">Product Name</th>
            <th scope="col">Unit</th>
            <th scope="col">Supplier Code</th>
            <th scope="col">Category Code</th>
            <th scope="col">Group Code</th>
            <th scope="col">Pattern Code</th>
            <th scope="col">Design Code</th>
            <th scope="col">Brand</th>
            <th scope="col">Supplier Name</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>
                <select class="form-select" aria-label="point_type" name="point_type" id="point_type">
                    <option>-- Select Point Type --</option>
                    <option value="HIP" selected>HIP</option>
                    <option value="Structure">Structure</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="point_type" name="product_type" id="product_type">
                    <option>-- Select Prodcut Type --</option>
                    <option value="Local" selected>Local</option>
                    <option value="Foregin">Foregin</option>
                </select>
            </td>
            <td><input class="form-control" type="number" name="product_code" id="product_code" value="2000000302454" />
            </td>
            <td><textarea type="text" name="product_name" id="product_name"
                    class="form-control">Aladdin UPVC Fixe & Casement Window 80mm Frame (W)2ft x (H)5ft Sola Green (CM)</textarea>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="unit" id="unit">
                    <option>-- Select Unit --</option>
                    <option value="PC" selected>PC</option>
                    <option value="Bag">Bag</option>
                </select>
            </td>
            <td> <select class="form-select" aria-label="Unit" name="supplier_code" id="supplier_code">
                    <option>-- Select Supplier Code --</option>
                    <option value="VEN-001061" selected>VEN-001061</option>
                    <option value="VEN-001062">VEN-001062</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="category_code" id="category_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW" selected>08-DW</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="group_code" id="group_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW06" selected>08-DW06</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="pattern_code" id="pattern_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW0601" selected>08-DW0601</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="design_code" id="design_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW060106" selected>08-DW060106</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="brand_name" id="brand_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="Aladdin" selected>Aladdin</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="vendor_name" id="vendor_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="U MYINT THU Construction Group Co;Ltd" selected>U MYINT THU Construction Group
                        Co;Ltd</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td><button class="btn btn-success">Insert</button></td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>
                <select class="form-select" aria-label="point_type" name="point_type" id="point_type">
                    <option>-- Select Point Type --</option>
                    <option value="HIP" selected>HIP</option>
                    <option value="Structure">Structure</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="point_type" name="product_type" id="product_type">
                    <option>-- Select Prodcut Type --</option>
                    <option value="Local" selected>Local</option>
                    <option value="Foregin">Foregin</option>
                </select>
            </td>
            <td><input class="form-control" type="number" name="product_code" id="product_code" value="2000000302454" />
            </td>
            <td><textarea type="text" name="product_name" id="product_name"
                    class="form-control">Aladdin UPVC Fixe & Casement Window 80mm Frame (W)2ft x (H)5ft Sola Green (CM)</textarea>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="unit" id="unit">
                    <option>-- Select Unit --</option>
                    <option value="PC" selected>PC</option>
                    <option value="Bag">Bag</option>
                </select>
            </td>
            <td> <select class="form-select" aria-label="Unit" name="supplier_code" id="supplier_code">
                    <option>-- Select Supplier Code --</option>
                    <option value="VEN-001061" selected>VEN-001061</option>
                    <option value="VEN-001062">VEN-001062</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="category_code" id="category_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW" selected>08-DW</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="group_code" id="group_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW06" selected>08-DW06</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="pattern_code" id="pattern_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW0601" selected>08-DW0601</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="design_code" id="design_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW060106" selected>08-DW060106</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="brand_name" id="brand_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="Aladdin" selected>Aladdin</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="vendor_name" id="vendor_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="U MYINT THU Construction Group Co;Ltd" selected>U MYINT THU Construction Group
                        Co;Ltd</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td><button class="btn btn-success">Insert</button></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>
                <select class="form-select" aria-label="point_type" name="point_type" id="point_type">
                    <option>-- Select Point Type --</option>
                    <option value="HIP" selected>HIP</option>
                    <option value="Structure">Structure</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="point_type" name="product_type" id="product_type">
                    <option>-- Select Prodcut Type --</option>
                    <option value="Local" selected>Local</option>
                    <option value="Foregin">Foregin</option>
                </select>
            </td>
            <td><input class="form-control" type="number" name="product_code" id="product_code" value="2000000302454" />
            </td>
            <td><textarea type="text" name="product_name" id="product_name"
                    class="form-control">Aladdin UPVC Fixe & Casement Window 80mm Frame (W)2ft x (H)5ft Sola Green (CM)</textarea>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="unit" id="unit">
                    <option>-- Select Unit --</option>
                    <option value="PC" selected>PC</option>
                    <option value="Bag">Bag</option>
                </select>
            </td>
            <td> <select class="form-select" aria-label="Unit" name="supplier_code" id="supplier_code">
                    <option>-- Select Supplier Code --</option>
                    <option value="VEN-001061" selected>VEN-001061</option>
                    <option value="VEN-001062">VEN-001062</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="category_code" id="category_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW" selected>08-DW</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="group_code" id="group_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW06" selected>08-DW06</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="pattern_code" id="pattern_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW0601" selected>08-DW0601</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="design_code" id="design_code">
                    <option>-- Select Category Code --</option>
                    <option value="01-CB">01-CB</option>
                    <option value="08-DW060106" selected>08-DW060106</option>
                    <option value="03-RT">03-RT</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="brand_name" id="brand_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="Aladdin" selected>Aladdin</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td>
                <select class="form-select" aria-label="Unit" name="vendor_name" id="vendor_name">
                    <option>-- Select Category Code --</option>
                    <option value="UPG">UPG</option>
                    <option value="U MYINT THU Construction Group Co;Ltd" selected>U MYINT THU Construction Group
                        Co;Ltd</option>
                    <option value="Cotto">Cotto</option>
                </select>
            </td>
            <td><button class="btn btn-success">Insert</button></td>
        </tr>
    </tbody>
</table>
@endsection
