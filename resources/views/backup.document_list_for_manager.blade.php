<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="p-5">
    @if($errors->any())
    <div class="bg-info p-3">

        @foreach($errors->all() as $error)
        <div class="bg-light p-3 mb-3 card">

            @php
            $script_line_arr = explode("<br>",$error);
            @endphp
            @foreach($script_line_arr as $script_line)

            <div class="form-group">

                <div class="input-group input-group-copy">
                    <span class="input-group-btn">
                        <button class="btn btn-success">Copy <i class="fas fa-copy"></i></button>
                    </span>

                    <input id="test" type="text" readonly class="form-control" value="{{ $script_line }}" />
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    </div>
    @endif
    <div class="bg-light p-3">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif

        @role('Manager')
        @if(count($documents) >= 1)

        <h2 class="text-info text-center p-2 m-2"> Requested Document</h2>

        <table class="table table-bordered">
            <tr>
                <th>Document No</th>
                <th>Product Code</th>
                <th>Prepared By</th>
                <th>Prepared At</th>
                <th>Approved By</th>
                <th>Approved At</th>
                <th>Exported By</th>
                <th>Exported At</th>
            </tr>
            @foreach ($documents as $document)
            <tr>
                <td> <a class="" href="{{ route('approve_view',$document->id) }}">{{ $document->document_no}}</a></td>

                <td>{{ $document->pcode_id }}</td>
                <td>{{ $document->prepared->name }}</td>
                <td>{{ $document->prepared_at }}</td>
                <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
                <td>{{ $document->approved_at }}</td>
                <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
                <td>{{ $document->exported_at}}</td>
            </tr>
            @endforeach
        </table>
        @endif
        @endrole
        @if(count($approved) >= 1 )
        <h2 class="text-info text-center p-2 m-2"> Approved Document</h2>
        <table class="table table-bordered">
            <tr>
                <th>Document No</th>
                <th>Product Code</th>
                <th>Prepared By</th>
                <th>Prepared At</th>
                <th>Approved By</th>
                <th>Approved At</th>
                <th>Exported By</th>
                <th>Exported At</th>
                @can('sd-team-save')
                <th>Action</th>
                @endcan

            </tr>
            @foreach ($approved as $document)
            <tr>
                <td>{{ $document->document_no}}</td>
                <td>{{ $document->pcode_id }}</td>
                <td>{{ $document->prepared->name }}</td>
                <td>{{ $document->prepared_at }}</td>
                <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
                <td>{{ $document->approved_at }}</td>
                <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
                <td>{{ $document->exported_at}}</td>
                @can('sd-team-save')
                <td>
                    <form action="{{ route('doc_export',$document->id) }}" method="POST">
                        @csrf
                        @method('POST')
                        <a class="btn btn-info open pr-3" href="{{ route('export_script',$document->id) }}">Export <i class="fas fa-file-export"></i></a>
                        <button type="submit" class="btn btn-success">Done <i class="fas fa-database"></i></button>
                    </form>
                </td>
                @endcan


            </tr>
            @endforeach
        </table>
        @endif
        @if(count($done_docs) >= 1 )
        <h2 class="text-info text-center p-2 m-2"> Exported Document</h2>
        <table class="table table-bordered">
            <tr>
                <th>Document No</th>
                <th>Product Code</th>
                <th>Prepared By</th>
                <th>Prepared At</th>
                <th>Approved By</th>
                <th>Approved At</th>
                <th>Exported By</th>
                <th>Exported At</th>
                @can('sd-team-save')
                <th>Action</th>
                @endcan
            </tr>
            @foreach ($done_docs as $document)
            <tr>
                <td>{{ $document->document_no}}</td>
                <td>{{ $document->pcode_id }}</td>
                <td>{{ $document->prepared->name }}</td>
                <td>{{ $document->prepared_at }}</td>
                <td>{{ $document->approved_by ? $document->approved->name : "" }}</td>
                <td>{{ $document->approved_at }}</td>
                <td>{{ $document->exported_by ? $document->exported->name : ""}}</td>
                <td>{{ $document->exported_at}}</td>
                @can('sd-team-save')
                <td>
                    <a class="btn btn-info open" href="{{ route('export_script',$document->id) }}">Export <i class="fas fa-file-export"></i></a>
                </td>
                @endcan

            </tr>
            @endforeach
        </table>
        @endif
    </div>
</div>


@endsection
@section('js')
<script>
    var inputCopyGroups = document.querySelectorAll('.input-group-copy');

    for (var i = 0; i < inputCopyGroups.length; i++) {
        var _this = inputCopyGroups[i];
        var btn = _this.getElementsByClassName('btn')[0];
        var input = _this.getElementsByClassName('form-control')[0];

        input.addEventListener('click', function(e) {
            this.select();
        });
        btn.addEventListener('click', function(e) {
            var input = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
            input.select();
            try {
                var success = document.execCommand('copy');
                console.log('Copying ' + (success ? 'Succeeded' : 'Failed'));
            } catch (err) {
                console.log('Copying failed');
            }
        });
    }
</script>
@endsection