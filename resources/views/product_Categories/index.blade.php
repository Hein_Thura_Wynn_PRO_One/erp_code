@extends('adminlte::page')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Product Category</h2>
        </div>
        <div class="pull-right mb-3">
            @can('product-category-create')
            <a class="btn btn-success" href="{{ route('product_Categories.create') }}"> Add New Category</a>
            @endcan
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Product Category Id</th>
        <th>Product Category Code</th>
        <th>Product Category Name</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($product_Categories as $product_Category)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $product_Category->product_category_id }}</td>
        <td>{{ $product_Category->product_category_code }}</td>
        <td>{{ $product_Category->product_category_name }}</td>
      
        <td>
            <form action="{{ route('product_Categories.destroy',$product_Category->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('product_Categories.show',$product_Category->id) }}">Show</a>
                @can('product-category-edit')
                <a class="btn btn-primary" href="{{ route('product_Categories.edit',$product_Category->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('product-category-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>


{!! $product_Categories->links() !!}


@endsection