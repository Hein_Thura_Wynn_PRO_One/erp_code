@extends('adminlte::page')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Product Group</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('product_Codes.index') }}"> Back</a>
        </div>
    </div>
</div>
@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{ route('product_Codes.update',$product_Code->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Product Type <span class="text-red">*</span>: </label>
                <select class="form-control" name="type">
                    @if ($product_Code->type == 0)
                    <option value="0" selected>{{ 'HIP' }}</option>
                    <option value="1">{{ 'STRUCTURE' }}</option>
                    @elseif($product_Code->type == 1)
                    <option value="0">{{ 'HIP' }}</option>
                    <option value="1" selected>{{ 'STRUCTURE' }}</option>
                    @else
                    <option value="0">{{ 'HIP' }}</option>
                    <option value="1">{{ 'STRUCTURE' }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Code No:</strong>
                <input type="text" name="product_code_no" value="{{ $product_Code->product_code_no }}" class="form-control" placeholder="Product Code No">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Product Name:</strong>
                <input type="text" name="product_name" value="{{ $product_Code->product_name }}" class="form-control" placeholder="Product Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Category Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='category_id' name="{{'category_id'}}">
                    @if($categories != null)
                    @foreach ($categories as $ln)
                    <option value="{{$ln->id}}" {{ ( $ln->id == $product_Code->category_id  ? 'selected' : '') }}>{{ $ln->product_category_name }}</option>
                    @endforeach
                    @elseif ($category != null)
                    <option value="{{$category->id}}" {{  $product_Code->category_id == $category->id ? 'selected': '' }}>{{ $category->product_category_name }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Group Name <span class="text-red">*</span>: </label>
                <select class="form-control" id='group_id' name="{{'group_id'}}">
                    <option value="{{$product_Code->group_id}}" {{  $product_Code->group_id ? 'selected': '' }}>{{$product_Code->groups->product_group_name}} </option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Pattern Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='pattern_id' name="{{'pattern_id'}}">
                    <option value="{{$product_Code->pattern_id}}" {{  $product_Code->pattern_id ? 'selected': '' }}>{{ $product_Code->pattern_id ? $product_Code->patterns->product_pattern_name  : 'Uncategorized' }} </option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Design Name <span class="text-red">*</span>:</label>
                <select class="form-control" id='design_id' name="{{'design_id'}}">
                    <option value="{{$product_Code->design_id}}" {{  $product_Code->design_id ? 'selected': '' }}>{{ $product_Code->design_id ? $product_Code->designs->product_design_name  : 'Uncategorized' }}</option>

                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Unit Name <span class="text-red">*</span>:</label>
                <select class="form-control" name="{{'unit_id'}}">
                    @foreach ($units as $ln)
                    <option value="{{$ln->id}}" {{ ($ln->id == $ln->unit_id  ? 'selected="selected"' : '') }}>{{ $ln->product_unit_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Vendor Name <span class="text-red">*</span>:</label>
                <select class="form-control" name="{{'supplier_id'}}">

                    @foreach ($vendors as $ln)
                    <option value="{{$ln->id}}" {{ ($ln->id == $ln->supplier_id  ? 'selected="selected"' : '') }}> {{ $ln->vendor_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Brand Name <span class="text-red">*</span>:</label>
                <select class="form-control" name="{{'brand_id'}}">
                    @foreach ($brands as $ln)
                    <option value="{{$ln->id}}" {{ ($ln->id == $ln->branch_id  ? 'selected="selected"' : '') }}> {{ $ln->product_brand_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label class="control-label col-sm-4 pull-left"> Product Pack Flag<span class="text-red">*</span>: </label>
                <select class="form-control" name="product_pack_flag">
                    @if ($product_Code->product_pack_flag == 0)
                    <option value="0" selected>{{ 'Product Code' }}</option>
                    <option value="1">{{ 'GP' }}</option>
                    <option value="2">{{ 'FOC' }}</option>
                    @elseif($product_Code->product_pack_flag == 1)
                    <option value="0">{{ 'Product Code' }}</option>
                    <option value="1" selected>{{ 'GP' }}</option>
                    <option value="2">{{ 'FOC' }}</option>
                    @elseif($product_Code->product_pack_flag == 2)
                    <option value="0">{{ 'Product Code' }}</option>
                    <option value="1">{{ 'GP' }}</option>
                    <option value="2" selected>{{ 'FOC' }}</option>
                    @else
                    <option value="0">{{ 'Product Code' }}</option>
                    <option value="1">{{ 'GP' }}</option>
                    <option value="1">{{ 'FOC' }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        var url = window.location.origin;
        $('#category_id').click(function() {
            var id = $(this).val();
            // $('#group_id').find('option').not(':first').remove();
            $('#group_id').find('option').remove();
            $.ajax({
                url: url + '/product_Codes/cat/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_group_id;
                            var name = response.data[i].product_group_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";

                            $("#group_id").append(option);
                        }
                    }
                }
            })
        });
        $('#group_id').click(function() {
            var id = $(this).val();
            $('#pattern_id').find('option').remove();
            $.ajax({
                url: url + '/product_Codes/gp/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_pattern_id;
                            var name = response.data[i].product_pattern_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#pattern_id").append(option);
                        }
                    }
                }
            })
        });
        $('#pattern_id').click(function() {
            var id = $(this).val();
            $('#design_id').find('option').remove();
            $.ajax({
                url: url + '/product_Codes/ptn/' + id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var len = 0;
                    if (response.data != null) {
                        len = response.data.length;
                    }
                    if (len > 0) {
                        for (var i = 0; i < len; i++) {
                            var id = response.data[i].product_design_id;
                            var name = response.data[i].product_design_name;
                            // console.log(name);
                            var option = "<option value='" + id + "'>" + name + "</option>";
                            $("#design_id").append(option);
                        }
                    }
                }
            })
        });
    });
</script>
@endsection