<!DOCTYPE html>
@extends('layouts.app')

@section('content')


@if ($message = Session::get('success'))
<div class="bg-info p-3">
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
</div>
@endif


<!-- Datatable for Checker -->
@role('Checker')
<div class="p-3">

    <div class="row">
        <div class="col-md-12  text-center py-3 my-2 bg-light">
            <h1 class="text-uppercase align-center m-auto">Document List</h1>
        </div>
    </div>
    <div class="bg-light mt-5 p-3">


        <table class="table table-bordered data-table" id="data_table">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>Document No</th>
                    <th>Category</th>
                    <th>Item</th>
                    <th>Prepared By</th>
                    <th>Prepared At</th>
                    <th>Checked By</th>
                    <th>Checked At</th>
                    <th>Approved By</th>
                    <th>Approved At</th>
                    <th>Exported By</th>
                    <th>Exported At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endrole
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        var inputCopyGroups = document.querySelectorAll('.input-group-copy');
        for (var i = 0; i < inputCopyGroups.length; i++) {
            var _this = inputCopyGroups[i];
            var btn = _this.getElementsByClassName('btn')[0];
            var input = _this.getElementsByClassName('form-control')[0];
            input.addEventListener('click', function(e) {
                this.select();
            });
            btn.addEventListener('click', function(e) {
                var input = this.parentNode.parentNode.getElementsByClassName('form-control')[0];
                input.select();
                try {
                    var success = document.execCommand('copy');
                    console.log('Copying ' + (success ? 'Succeeded' : 'Failed'));
                } catch (err) {
                    console.log('Copying failed');
                }
            });
        }
        /////Datatable////
        $(function() {
            var table = $('#data_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('document_list_for_checker') }}",
                columns: [{
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'document_no',
                        name: 'document_no'
                    },
                    {
                        data: 'category_name',
                        name: 'category_name'
                    },
                    {
                        data: 'pcode_id',
                        name: 'pcode_id'
                    },
                    {
                        data: 'prepared_by',
                        name: 'prepared_by'
                    },
                    {
                        data: 'prepared_at',
                        name: 'prepared_at'
                    },
                    {
                        data: 'checked_by',
                        name: 'checked_by'
                    },
                    {
                        data: 'checked_at',
                        name: 'checked_at'
                    },
                    {
                        data: 'approved_by',
                        name: 'approved_by'
                    },
                    {
                        data: 'approved_at',
                        name: 'approved_at'
                    },
                    {
                        data: 'exported_by',
                        name: 'exported_by'
                    },
                    {
                        data: 'exported_at',
                        name: 'exported_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    });
</script>

@endsection