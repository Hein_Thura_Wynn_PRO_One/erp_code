@extends('layouts.app')
@section('content')
<div class="container bg-light">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Departments</h2>
        </div>
        <div class="pull-right mb-3">
            @can('customer-create')
            <a class="btn btn-success" href="{{ route('customers.create') }}"> Add New Customer Record</a>
            @endcan
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Branch Code</th>
        <th>Date Server</th>
        <th>Date Client</th>
        <th>Employee Code</th>
        <th>Number of Customer</th>
    </tr>
    @foreach ($customers as $customer)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $customer->branch_code }}</td>
        <td>{{ $customer->date_server }}</td>
        <td>{{ $customer->date_client }}</td>
        <td>{{ $customer->empl_code }}</td>
        <td>{{ $customer->number_of_customer }}</td>  
    </tr>
    @endforeach
</table>
</div>
@endsection