@extends('adminlte::page')

@section('content')
<div class="row">
	<div class="col-lg-12 margin-tb">
		<div class="pull-left">
			<h2>Edit branch</h2>
		</div>
		<div class="pull-right">
			<a class="btn btn-primary" href="{{ route('branches.index') }}"> Back</a>
		</div>
	</div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
	<strong>Whoops!</strong> There were some problems with your input.<br><br>
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form action="{{ route('branches.update',$branch->id) }}" method="POST">
	@csrf
	@method('PUT')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Name:</strong>
				<input type="text" name="name" value="{{ $branch->name }}" class="form-control" placeholder="Name">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Branch ID:</strong>
				<input type="text" name="branch_id" value="{{ $branch->branch_id }}" class="form-control" placeholder="Branch ID">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Short Nane:</strong>
				<input type="text" name="short_name" value="{{ $branch->short_name }}" class="form-control" placeholder="Short Name">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Branch Active:</strong>
				<input type="text" name="branch_active" value="{{ $branch->branch_active }}" class="form-control" placeholder="Branch Active">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Address:</strong>
				<input type="text" name="address" value="{{ $branch->address }}" class="form-control" placeholder="Address">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Phone No:</strong>
				<input type="text" name="phone_no" value="{{ $branch->phone_no }}" class="form-control" placeholder="Phone_no">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Email:</strong>
				<input type="text" name="email" value="{{ $branch->email }}" class="form-control" placeholder="Email">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Facebook Link:</strong>
				<input type="text" name="fb_link" value="{{ $branch->fb_link }}" class="form-control" placeholder="fb_link">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	</div>
</form>

@endsection