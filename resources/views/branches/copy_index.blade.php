@extends('adminlte::page')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>branches</h2>
        </div>
        <div class="pull-right mb-3">
            @can('branch-create')
            <a class="btn btn-success" href="{{ route('branches.create') }}"> Add New Branch</a>
            @endcan
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<!-- ////For Search Box/// -->
<!-- <div class="row">
    <div class="col-lg-9"></div>
    <div class="col-lg-3">

        <div class="form-group">
            <label>Type a branch name</label>
            <input type="text" name="name" id="name" placeholder="Enter branch name" class="form-control">
        </div>
        <div id="branch_list"></div>
    </div>
</div> -->

<!-- <div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3><strong>Laravel 8 Datatable Example</strong></h3>
                </div>
            </div>
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th width="50">No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div> -->
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3><strong>Laravel 8 Datatable Example</strong></h3>
                </div>
            </div>
            <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th width="50">No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- <table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Short Name</th>
        <th>Address</th>
        <th>Phone No</th>
        <th>Email</th>
        <th>Facebook Link</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($branches as $branch)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $branch->name }}</td>
        <td>{{ $branch->short_name }}</td>
        <td>{{ $branch->address }}</td>
        <td>{{ $branch->phone_no }}</td>
        <td>{{ $branch->email }}</td>
        <td>{{ $branch->fb_link }}</td>

        <td>
            <form action="{{ route('branches.destroy',$branch->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('branches.show',$branch->id) }}">Show</a>
                @can('branch-edit')
                <a class="btn btn-primary" href="{{ route('branches.edit',$branch->id) }}">Edit</a>
                @endcan


                @csrf
                @method('DELETE')
                @can('branch-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table> -->


{!! $branches->links() !!}


@endsection


{{--  @section('js')
<script type="text/javascript">
    $(document).ready(function() {

        $('#name').on('keyup', function() {
            var query = $(this).val();
            
            $.ajax({

                url: "{{ route('branches.search') }}",

                type: "GET",

                data: {
                    'branch': query
                },  

                success: function(data) {

                    $('#branch_list').html(data);
                }
            })
            // end of ajax call
        });


        $(document).on('click', 'li', function() {

            var value = $(this).text();
            $('#branch').val(value);
            $('#branch_list').html("");
        });
    });
</script>

@endsection  --}}

<script type="text/javascript">
  $(function () {
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('search.search') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
</script>