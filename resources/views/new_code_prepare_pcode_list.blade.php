@extends('new_code_prepare.blade')

@section('pcode_list')
<div class="alert alert-success">
<p>hi</p>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Type</th>
        <th>Product Code No</th>
        <th>Product Name</th>
        <th>Category</th>
        <th>Group</th>
        <th>Pattern</th>
        <th>Design</th>
        <th>Supplier</th>
        <th>Brand</th>
        <th>Unit</th>
        <th>Product Pack Flag</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($products as $product)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $product->type === 0 ? 'HIP' : 'Structure' }}</td>
        <td>{{ $product->product_code_no }}</td>
        <td>{{ $product->product_name }}</td>
        <td>{{ $product->category_id ? $product->categories->product_category_name : 'Uncategorized' }}</td>
        <td>{{ $product->group_id ? $product->groups->product_group_name : 'Uncategorized' }}</td>
        <td>{{ $product->pattern_id ? $product->patterns->product_pattern_name : 'Uncategorized' }}</td>
        <td>{{ $product->design_id ? $product->designs->product_design_name : 'Uncategorized' }}</td>
        <td>{{ $product->supplier_id ? $product->suppliers->vendor_name : 'Uncategorized' }}</td>
        <td>{{ $product->brand_id ? $product->brands->product_brand_name : 'Uncategorized' }}</td>
        <td>{{ $product->unit_id ? $product->units->product_unit_name : 'Uncategorized' }}</td>

        <td> @if ($product->product_pack_flag == 0)
            {{ 'G Product Code' }}
            @elseif($product->product_pack_flag == 1)
            {{ 'GP' }}
            @elseif($product->product_pack_flag == 2)
            {{ 'FOC' }}
            @else
            {{ 'S' }}
            {{ 'P' }}
            {{ 'G' }}
            @endif
        </td>
        <td>
            <form action="{{ route('product_Codes.destroy',$product->id) }}" method="POST">
                <a class="btn btn-info" href="{{ route('product_Codes.show',$product->id) }}">Show</a>
                @can('pcode-edit')
                <a class="btn btn-primary" href="{{ route('product_Codes.edit',$product->id) }}">Edit</a>
                @endcan

                @csrf
                @method('DELETE')
                @can('pcode-delete')
                <button type="submit" class="btn btn-danger">Delete</button>
                @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>

@endsection