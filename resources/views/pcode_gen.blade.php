<!DOCTYPE html>
@extends('layouts.app')

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="offset-3 col-6">

        <form action="{{ route('pcode_man_get') }}" method="POST" class="form-group bg-light p-3 m-3">
            @csrf
            <h1 class="text-center">{{'Generate Code Manual' }} </h1>
            <div class="row mt-3">
                <div class="col-9">
                    <input type="number" min=1 class="form-control" name="limit" class="form-control " placeholder="No of code : 1" />
                </div>
                <div class="col-3">
                    <button type="submit" id="save_doc" class="btn btn-primary form-control">{{ 'Generate' }} <i class="fas fa-file"></i></button>

                </div>

            </div>

        </form>

    </div>
</div>

@endsection