@extends('adminlte::page')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2></h2>
        </div>
        <div class="pull-right mb-3">
            @can('pcode-create')
            <a class="btn btn-success" href="{{ route('documents.create') }}"> Create New Document</a>
            @endcan
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create_product_code">
            Launch demo modal
        </button>
        <div id="create_product_code" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="create_product_codeLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="create_product_codeLabel">Create New Document</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('product_Codes.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="offset-xs-10 offset-sm-10  offset-md-10 col-xs-2 col-sm-2 col-md-2">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Save <i class="fa fab fa-save"></i></button>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Name :</label>
                                        <input type="text" name="product_name" class="form-control" value="{{ old('product_name') }}" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Prodcut Code:</label>
                                        <input type="text" name="product_code_no" class="form-control" value="{{ old('product_code_no') }}" placeholder="Barcode">
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Product Type <span class="text-red">*</span>:</label>
                                        <select class="form-control" name="type">
                                            <option>{{ '--HIP ? Structure--' }}</option>
                                            @if (old('type') == 0)
                                            <option value="0" selected>{{ 'HIP' }}</option>
                                            <option value="1">{{ 'STRUCTURE' }}</option>
                                            @elseif(old('type') == 1)
                                            <option value="0">{{ 'HIP' }}</option>
                                            <option value="1" selected>{{ 'STRUCTURE' }}</option>
                                            @else
                                            <option value="0">{{ 'HIP' }}</option>
                                            <option value="1">{{ 'STRUCTURE' }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Category Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" id='category_id' name="{{'category_id'}}">
                                            <option>{{ '--Select One Category--' }}</option>
                                            @if($categories != null)
                                            @foreach ($categories as $ln)
                                            <option value="{{$ln->id}}" {{ old('category_id') == $ln->id ? 'selected': '' }}>{{ $ln->product_category_name }}</option>
                                            @endforeach
                                            @elseif ($category != null)
                                            <option value="{{$category->id}}" {{ old('category_id') == $category->id ? 'selected': '' }}>{{ $category->product_category_name }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Group Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" id='group_id' name="{{'group_id'}}">
                                            <option value="0">{{ '--Select One Group--' }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Pattern Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" id='pattern_id' name="{{'pattern_id'}}">
                                            <option value="0">{{ '--Select One Pattern--' }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Design Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" id='design_id' name="{{'design_id'}}">
                                            <option value="0">{{ '--Select One Design--' }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Unit Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" name="{{'unit_id'}}">
                                            <option>{{ '--Select One Unit--' }}</option>
                                            @foreach ($units as $ln)
                                            <option value="{{$ln->id}}">{{ $ln->product_unit_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Vendor Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" name="{{'supplier_id'}}">
                                            <option>{{ '--Select One Vendor--' }}</option>
                                            @foreach ($vendors as $ln)
                                            <option value="{{$ln->id}}">{{ $ln->vendor_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4 pull-left"> Brand Name <span class="text-red">*</span>:</label>
                                        <select class="form-control" name="{{'brand_id'}}">
                                            <option>{{ '--Select One Brand--' }}</option>
                                            @foreach ($brands as $ln)
                                            <option value="{{$ln->id}}">{{ $ln->product_brand_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>Document No</th>
        <th>Product Code</th>
        <th>Prepared By</th>
        <th>Approved By</th>
        <th>Created By</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($documents as $document)
    <tr>
        <td>{{ $document->document_no}}</td>
        <td>{{ $document->pcode_id}}</td>
        <td>{{ $document->prepared_by}}</td>
        <td>{{ $document->approved_by}}</td>
        <td>{{ $document->created_by}}</td>
        <td>
        </td>
    </tr>
    @endforeach
</table>



@endsection