<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Depts;
use App\Models\Mercat;
use App\Models\User;
use Dotenv\Validator;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'DESC')->paginate(50);
        return view('users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 50);
    }

    ////// DropdownAjax//////
    public function getdept($branch_id)
    {
        $data = Depts::where('branch_id', $branch_id)->get();
        Log::info($data);
        return response()->json(['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        $branches = Branch::all();
        $mercats = Mercat::all();
        $depts = Depts::all();
        return view('users.create', compact('roles', 'branches', 'depts', 'mercats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'nullable',
            'email' => 'nullable|email|unique:users,email',
            'ph_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9|max:11|unique:users,ph_no',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',
            'employee_id' => 'required|regex:/^\d{3}(-\d{6})?$/',
            
        ]);

        $input = $request->all();
        if (!empty($input['mercate_id'])) {
            $input['mercate_id'] = implode(',', $input['mercate_id']);
        }
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Role::pluck('name', 'name')->all();
        $user = User::find($id);
        $branches = Branch::all();
        $mercats = Mercat::all();
        $depts = Depts::all();
        return view('users.show', compact('user', 'roles', 'branches', 'depts', 'mercats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        $branches = Branch::all();
        $mercats = Mercat::all();
        $depts = Depts::all();
        return view('users.edit', compact('user', 'roles', 'userRole', 'branches', 'depts', 'mercats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        $this->validate($request, [
            'name' => 'required',
            'password' => 'same:confirm-password',
            'roles' => 'required',
            'employee_id' => 'required'

        ]);
        // 'email' => 'required|email|unique:users,email,' . $id,
        $input = $request->all();

        if (!empty($input['mercate_id'])) {
            $input['mercate_id'] = implode(',', $input['mercate_id']);
        }
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }
}
