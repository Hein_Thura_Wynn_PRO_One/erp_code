<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Field;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:field-create', ['only' => ['index', 'show']]);
        $this->middleware('permission:field-create', ['only' => ['create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fields  = Field::latest()->paginate(5);
        return view('fields.index', compact('fields'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $branches = Branch::all();
        $fields = Field::all();
        return view('fields.create', compact('fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'name' => 'required',

        ]);

        // $customer = Auth::id();
        // $u_obj = User::select('employee_id')->where('id', $customer)->first();
        // $empl_code = $u_obj->employee_id;
        // $request['date_server'] = now();
        // $request['empl_code'] = $empl_code;
        Field::create($request->all());
        return redirect()->route('fields.index')
            ->with('success', 'Field record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function show(Field $field)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function edit(Field $field)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Field $field)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function destroy(Field $field)
    {
        //
    }
}
