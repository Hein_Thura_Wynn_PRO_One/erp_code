<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Customer;
use App\Models\Field;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:customer-create', ['only' => ['index', 'show']]);
        $this->middleware('permission:customer-create', ['only' => ['create', 'store']]);
      
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::latest()->paginate(5);
        return view('customers.index', compact('customers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = Branch::all();
        $time =  [
            '0'=> '08:00AM - 09:00AM',
            '1'=> '09:00AM - 10:00AM',
            '2'=> '10:00AM - 11:00AM',
            '3'=> '11:00AM - 12:00PM',
            '4'=> '12:00PM - 01:00PM',
            '5'=> '01:00PM - 02:00PM',
            '6'=> '02:00PM - 03:00PM',
            '7'=> '03:00PM - 04:00PM',
            '8'=> '04:00PM - 05:00PM',
            '9'=> '05:00PM - 06:00PM',
            '10'=> '06:00PM - 07:00PM'
        ];
        // $fileds = Field::all();
        return view('customers.create', compact('branches','time'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        request()->validate([
            'branch_code' => 'required',
            'date_client' => 'required',
            'number_of_customer' => 'required|integer',
        ]);

        $customer = Auth::id();
        $u_obj = User::select('employee_id')->where('id', $customer)->first();
        $empl_code = $u_obj->employee_id;
        $request['date_server'] = now();
        $request['empl_code'] = $empl_code;
        Customer::create($request->all());
        return redirect()->route('customers.index')
            ->with('success', 'Customer record created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
