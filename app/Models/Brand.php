<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'brand';
    protected $fillable = [
        'product_brand_code', 'product_brand_name'
        // 'name', 'short_desc', 'img', 'link', 'created_by'
    ];
}
