<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Pattern extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'product_patterns';
    protected $fillable = [
       'product_group_id', 'product_pattern_id', 'product_pattern_code', 'product_pattern_name'
    ];
}
