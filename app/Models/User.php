<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'ph_no',
        'email',
        'nrc',
        'dob',
        'address',
        'password',
        'branch_id',
        'dept_id',
        'mercate_id',
        'employee_id',
        'otp',
        'otp_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *r
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'otp_verified_at' => 'datetime',
    ];
    public function branches(){
        return $this->belongsTo(Branch::class,'branch_id');

    }
    public function depts(){
        return $this->belongsTo(Depts::class,'dept_id');
        
    }
    public function mercat(){
        return $this->belongsTo(MarCat::class,'mercate_id');
        
    }
    // depats

}
