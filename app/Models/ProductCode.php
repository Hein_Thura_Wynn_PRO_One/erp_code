<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCode extends Model
{
    use HasFactory;
    protected $table = 'product_codes';

    protected $fillable = [
        'type',
        'product_code_no',
        'product_grade_id',
        'product_name',
        'category_id',
        'group_id',
        'pattern_id',
        'design_id',
        'unit_id',
        'brand_id',
        'supplier_id',
        'product_pack_flag',
        'doc_no'
        
    ];
    public function categories(){
        return $this->belongsTo(Product_Category::class,'category_id');

    }

    public function groups(){
        return $this->belongsTo(Product_Group::class,'group_id');
        
    }
    public function patterns(){
        return $this->belongsTo(Product_Pattern::class,'pattern_id');
        
    }
    public function designs(){
        return $this->belongsTo(Product_Design::class,'design_id');
        
    }

    public function units(){
        return $this->belongsTo(Unit::class,'unit_id');
        
    }
    public function brands(){
        return $this->belongsTo(Brand::class,'brand_id');
        
    } 
    public function suppliers(){
        return $this->belongsTo(Supplier::class,'supplier_id');
        
    }

}
