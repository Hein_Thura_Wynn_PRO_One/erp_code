<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleHasPermissions extends Model
{
    //without Increment ID
    protected $primaryKey = null;
    public $incrementing = false;

    //created at and updated at
    public $timestamps = false;

    // use HasFactory;
    protected $fillable = [
        'permission_id',
        'role_id'
    ];
}
