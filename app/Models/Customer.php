<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Customer extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'branch_code',
        'date_server', 
        'date_client', 
        'empl_code', 
        'number_of_customer'
    ];

    public function branches(){
        return $this->belongsTo('App\Models\Branch','branch_code');
    }
}