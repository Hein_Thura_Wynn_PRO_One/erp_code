<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product_Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'product_categories';
    protected $fillable = [
        'product_category_id', 'product_category_code', 'product_category_name'
    ];
}
