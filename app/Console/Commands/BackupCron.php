<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BackupCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:backupdb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vendor_query = ' --command "insert into vendor (id,vendor_code,vendor_name) select * from vendor_from_erp where vendor_id not in (select id from vendor);"';
        $brand_query = ' --command "insert into brand (id, product_brand_code, product_brand_name) select * from brand_from_erp where product_brand_id not in (select id from brand);"';
        $category_query = ' --command "insert into product_categories (product_category_id, product_category_code, product_category_name) select * from category_from_erp where product_category_id not in (select product_category_id::integer from product_categories);"';
        $group_query = ' --command "insert into product_groups (product_group_id, product_category_id, product_group_code, product_group_name) select * from group_from_erp where product_group_id not in (select product_group_id from product_groups);"';
        $pattern_query = ' --command "insert into product_patterns (product_pattern_id, product_group_id, product_pattern_code, product_pattern_name) select * from pattern_from_erp where product_pattern_id not in (select product_pattern_id from product_patterns);"';
        $design_query = ' --command "insert into product_designs (product_design_id, product_pattern_id, product_design_code, product_design_name) select * from design_from_erp where product_design_id not in (select product_design_id from product_designs);"';
       
        Log::info("Cron is working fine!");

        $host = env("DB_HOST", "localhost");
        $user = env("DB_USERNAME", "postgres");
        $pwd = env("DB_PASSWORD", "password");
        $dbname = env("DB_DATABASE", "pgpos_global");
        $dir = "'/var/www/html/erp_code/db_backup/";
        $date = now();
        $now = $date->format('Y-F-j g-i-A');
        // DB connect Date
        
        // CMD
        $command = "export PGPASSWORD='" . $pwd . "'; " . "pg_dump -h " . $host . " -U" . $user . " -F c -b -v -f" . $dir . $dbname . $now . ".backup' " . $dbname . "  2>&1";
        $command2 = "&& find " . $dir . "' -name '*.backup' -type f -mtime +1 -delete;";
        $brand_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $brand_query;
        $vendor_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $vendor_query;
        $category_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $category_query;
        $group_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $group_query;
        $pattern_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $pattern_query;
        $design_cmd = "export PGPASSWORD='" . $pwd . "'; " . "psql -h " . $host . " -U" . $user . " --dbname=" . $dbname . $design_query;





        $command = $command . $command2;
        exec($command, $output, $return_var);
        Log::info("Backup is working fine!");

        exec($brand_cmd, $output, $return_var);// brand cron job
        Log::info("Brand Sync is working fine!");

        exec($vendor_cmd, $output, $return_var);// vendor cron job
        Log::info("Vendor Sync is working fine!");
      
        
        exec($category_cmd, $output, $return_var);// category cron job
        Log::info("Category Sync is working fine!");

        exec($group_cmd, $output, $return_var);// product group cron job
        Log::info("Product Group Sync is working fine!");

        exec($pattern_cmd, $output, $return_var);// product pattern cron job
        Log::info("Product Pattern Sync is working fine!");

        exec($design_cmd, $output, $return_var);// product pattern cron job
        Log::info("Product Design Sync is working fine!");

        //Something to write to txt log

        $this->info('Cron Cummand Run successfully!');
    }
}
